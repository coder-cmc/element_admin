import Layout from '@/layout'

const backwaterRouter = {
  path: '/backwater',
  component: Layout,
  redirect: '/backwater',
  name: 'backwater',
  meta: {
    title: '回水助手',
    icon: 'table'
  },
  children: [
    {
      path: 'index',
      component: () => import('@/views/backwater/index'),
      name: 'index',
      meta: { title: '回水设置' }
    },
    {
      path: 'jisuan',
      component: () => import('@/views/backwater/jisuan'),
      name: 'jisuan',
      meta: { title: '回水计算' }
    },
    {
      path: 'select',
      component: () => import('@/views/backwater/select'),
      name: 'select',
      meta: { title: '回水查询' }
    },
    {
      path: 'fandian_set',
      component: () => import('@/views/backwater/fandian_set'),
      name: 'fandian_set',
      meta: { title: '代理返点设置' }
    },
    {
      path: 'fandian_jisuan',
      component: () => import('@/views/backwater/fandian_jisuan'),
      name: 'fandian_jisuan',
      meta: { title: '代理返点计算' }
    },
    {
      path: 'baobiao',
      component: () => import('@/views/backwater/baobiao'),
      name: 'baobiao',
      meta: { title: '月统计报表' }
    },
    {
      path: 'daili_baobiao',
      component: () => import('@/views/backwater/daili_baobiao'),
      name: 'daili_baobiao',
      meta: { title: '代理月统计报表' }
    }
  ]
}
export default backwaterRouter
