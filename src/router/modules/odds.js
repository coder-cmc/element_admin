import Layout from '@/layout'

const oddsRouter = {
  path: '/odds',
  component: Layout,
  redirect: '/odds',
  name: 'odds',
  meta: {
    title: '赔率设置',
    icon: 'table'
  },
  children: [
    {
      path: 'type_28',
      component: () => import('@/views/odds/type_28'),
      name: 'type_28',
      meta: { title: 'PC类赔率设置' }
    },
    {
      path: 'pk10',
      component: () => import('@/views/odds/pk10'),
      name: 'pk10',
      meta: { title: '赛车类赔率设置' }
    },
    {
      path: 'k3',
      component: () => import('@/views/odds/k3'),
      name: 'k3',
      meta: { title: '快三类赔率设置' }
    },
    {
      path: 'ssc',
      component: () => import('@/views/odds/ssc'),
      name: 'ssc',
      meta: { title: '时时彩类赔率设置' }
    },
    // {
    //   path: 'ft',
    //   component: () => import('@/views/odds/ft'),
    //   name: 'ft',
    //   meta: { title: '翻摊类型' }
    // }
  ]
}
export default oddsRouter
