import Layout from '@/layout'

const financialRouter = {
  path: '/financial',
  component: Layout,
  redirect: '/financial/index',
  name: 'financial',
  meta: {
    title: '财务报表',
    icon: 'table'
  },
  children: [
    {
      path: 'index',
      component: () => import('@/views/financial/index'),
      name: 'DynamicTable',
      meta: { title: '上下分查询' }
    },
    {
      path: 'baobiao',
      component: () => import('@/views/financial/baobiao'),
      name: 'baobiao',
      meta: { title: '财务统计报表' }
    },
    {
      path: 'xiazhu',
      component: () => import('@/views/financial/xiazhu'),
      name: 'xiazhu',
      meta: { title: '玩家历史下注信息' }
    },
    {
      path: 'jifen',
      component: () => import('@/views/financial/jifen'),
      name: 'jifen',
      meta: { title: '玩家历史积分变动' }
    },
    {
      path: 'huishui',
      component: () => import('@/views/financial/huishui'),
      name: 'huishui',
      meta: { title: '回水历史查询' }
    }
  ]
}
export default financialRouter
