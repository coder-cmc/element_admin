const webMap = {
	"label": "前端",
	"prop": "web",
	"url": '',
	"link": "后台管理",
	"children": [{
			"label": "编程语言",
			"prop": "codeType",
			"disabled": true,
			"children": [{
					"label": "HTML",
					"prop": "HTML",
				},
				{
					"label": "CSS",
					"prop": "CSS",
				},
				{
					"label": "Javascript",
					"prop": "Javascript",
				},
			]
		},
		{
			"label": "JS框架",
			"prop": "jsFrame",
			"disabled": true,
			"children": [{
					"label": "Vue",
					"prop": "Vue",
				},
				{
					"label": "React",
					"prop": "React",
				},
				{
					"label": "Angular",
					"prop": "Angular",
					dicType: 'doc'
				},
			]
		},
		{
			"label": "UI框架",
			"prop": "uiFrame",
			"disabled": true,
			"url": '',
			"children": [{
					"label": "Element UI",
					"prop": "element_ui",
					"url": 'https://element.eleme.cn/#/zh-CN/component/i18n',
					"link": "官网",
				},
				{
					"label": "iview UI",
					"prop": "iview UI",
					"url": 'http://v1.iviewui.com/docs/introduce',
					"link": "官网",
				},
				{
					"label": "layUI",
					"prop": "layUI",
					"url": 'https://www.layui.com/doc/',
					"link": "官网",
				},
				{
					"label": "Ant Design",
					"prop": "Ant Design",
					"url": 'https://www.antdv.com/docs/vue/introduce-cn/',
					"link": "官网",
				},
			]
		},
	]
}

export default {
    webMap
}